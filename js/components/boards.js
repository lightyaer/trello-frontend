import $ from "jquery";

export const displayBoards = boards => {
  for (let board of boards) {
    $("#boards").append(`<div data-id="${board.id}" class="board">${board.name}</div>`);
  }
};

export const removeBoards = () => {
  $("#boards").remove();
};
