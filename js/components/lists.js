import $ from "jquery";

export const displayLists = lists => {
  $("#boards-section").hide();
  $("#lists-section").show();
  lists.forEach(list => {
    $("#lists").append(`<div data-id="${list.id}" class="list">${list.name}</div>`);
  });
};

export const removeLists = () => {
  $("#lists").remove();
};
