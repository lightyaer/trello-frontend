import $ from "jquery";
import {
  updateCheckListItemByCardIDAndChecklistItemID,
  deleteCheckListItemByCheckListItemID,
  createCheckListItemForCheckListID,
  getChecklistsByCardID
} from "../services";
export const displayChecklists = checklists => {
  $("#boards-section").hide();
  $("#lists-section").hide();
  $("#cards-section").hide();
  $("#checklists-section").show();
  $("#new-checklist-input").remove();
  $("#btn-add-checklist").remove();
  $(".checklist").remove();

  $("#checklists-section .header").after(`<input type="text" id="new-checklist-input" />
  <button id="btn-add-checklist" class="add-btn" data-cardid="${checklists[0].idCard}"  >Add Checklist</button>
  `);

  for (let checklist of checklists) {
    $("#checklists").append(`
    <div id="${checklist.id}" data-id="${checklist.id}" class="checklist">
    <h3 id="checklist-name" data-id="${checklist.id}" >${checklist.name}</h3>
    <button id="btn-delete-checklist" class="delete-btn floatRight" data-checklistid="${checklist.id}" >Delete Checklist</button>
    <input type="text" id="new-checklistItem-${checklist.id}"/>
    <button class="add-btn" data-cardid="${checklist.idCard}" data-checklistid="${checklist.id}" id="btn-add">Add</button>
     </div>
     `);

    for (let checkItem of checklist.checkItems) {
      let checkItemStr;
      if (checkItem.state === "incomplete") {
        checkItemStr = `<div id="${checkItem.id}" class="checkItem">
        <input id="checkbox" type="checkbox"
        data-cardid="${checklist.idCard}"
        data-name="${checkItem.name}"
        data-state="${checkItem.state}"
        data-id="${checkItem.id}"/>
        <label id="label-${checkItem.id}">${checkItem.name}</label>
        <div  class="checkItemButtons">
            <button class="edit-btn" data-state="${checkItem.state}" data-name="${checkItem.name}" data-cardid="${checklist.idCard}" data-id="${
          checkItem.id
        }" id="btn-edit">Edit</button>
            <button class="delete-btn" data-cardid="${checklist.idCard}" data-checklistid="${checkItem.idChecklist}" data-id="${
          checkItem.id
        }" id="delete">Delete</button>
        </div>
        </div>`;
      } else {
        checkItemStr = `<div id="${checkItem.id}" class="checkItem">
        <input id="checkbox" type="checkbox"
        data-id="${checkItem.id}" 
        data-cardid="${checklist.idCard}"
        data-name="${checkItem.name}"
        data-state="${checkItem.state}"
        checked/>
        <label id="label-${checkItem.id}">${checkItem.name}</label>
        <div class="checkItemButtons">
            <button class="edit-btn" data-state="${checkItem.state}" data-name="${checkItem.name}" data-cardid="${checklist.idCard}" data-id="${
          checkItem.id
        }" id="btn-edit">Edit</button>
            <button class="delete-btn" data-cardid="${checklist.idCard}" data-checklistid="${checkItem.idChecklist}" data-id="${
          checkItem.id
        }" id="delete">Delete</button>
        </div>
        </div>`;
      }
      $(`#checklists #${checklist.id}`).append(checkItemStr);
    }
  }
};

const addCheckItemToCheckList = (checkListItem, cardID) => {
  $(`#${checkListItem.idChecklist}`).append(`<div id="${checkListItem.id}" class="checkItem">
  <input id="checkbox" type="checkbox"
  data-cardid="${cardID}"
  data-name="${checkListItem.name}"
  data-state="${checkListItem.state}"
  data-id="${checkListItem.id}"/>
  <label id="label-${checkListItem.id}">${checkListItem.name}</label>
  <div  class="checkItemButtons">
      <button class="edit-btn" data-state="${checkListItem.state}" data-name="${checkListItem.name}" data-cardid="${cardID}" data-id="${
    checkListItem.id
  }" id="btn-edit">Edit</button>
      <button class="delete-btn" data-cardid="${cardID}" data-checklistid="${checkListItem.idChecklist}" data-id="${
    checkListItem.id
  }" id="delete">Delete</button>
  </div>
  </div>`);
};

export const addNewCheckListToChecklists = checklist => {
  $("#checklists").append(`
  <div id="${checklist.id}" data-id="${checklist.id}" class="checklist">
  <h3 id="checklist-name" data-id="${checklist.id}" >${checklist.name}</h3>
  <button id="btn-delete-checklist" class="delete-btn floatRight" data-checklistid="${checklist.id}" >Delete Checklist</button>
  <input type="text" id="new-checklistItem-${checklist.id}"/>
  <button class="add-btn" data-cardid="${checklist.idCard}" data-checklistid="${checklist.id}" id="btn-add">Add</button>
   </div>
  `);
};

export const checkBoxChangeHandler = async dataset => {
  $("#loader").show();
  dataset.state = dataset.state === "incomplete" ? "complete" : "incomplete";
  let cardID = dataset.cardid;
  let checklistItemID = dataset.id;
  let name = dataset.name;
  let state = dataset.state;
  await updateCheckListItemByCardIDAndChecklistItemID(cardID, checklistItemID, name, state);
  $("#loader").hide();
};

export const checkItemDeleteHandler = async dataset => {
  $("#loader").show();
  await deleteCheckListItemByCheckListItemID(dataset.checklistid, dataset.id);
  //DISPLAY UPDATED CHECKLISTS BECAUSE deleteCheckListItemByCheckListItemID doesn't return updated checklistItems
  let checklists = await getChecklistsByCardID(dataset.cardid);
  displayChecklists(checklists);
  $("#loader").hide();
};

export const checkItemAddHandler = async dataset => {
  $("#loader").show();
  let checklistID = dataset.checklistid;
  let addInput = $(`#new-checklistItem-${checklistID}`);
  let name = addInput.val();
  let checked = false;
  let checkItem = await createCheckListItemForCheckListID(checklistID, name, checked);
  let cardID = dataset.cardid;
  addInput.val("");
  addCheckItemToCheckList(checkItem, cardID);
  $("#loader").hide();
};

export const checkItemEditHandler = async dataset => {
  let checkItemDiv = $(`#${dataset.id}`);
  $(`#label-${dataset.id}`).remove();
  checkItemDiv.append(`<input id="edit-${dataset.id}" autofocus type="text"/>`);
  let editInput = $(`#edit-${dataset.id}`);
  editInput.val(dataset.name);

  editInput.change(async function(event) {
    $("#loader").show();
    let cardID = dataset.cardid;
    let checklistItemID = dataset.id;
    let name = event.target.value;
    let state = dataset.state;
    let checkItem = await updateCheckListItemByCardIDAndChecklistItemID(cardID, checklistItemID, name, state);
    $(`#edit-${dataset.id}`).remove();
    checkItemDiv.append(`<label id="label-${checkItem.id}" >${checkItem.name}</label>`);
    $("#loader").hide();
  });
};

export const checklistNameChangeHandler = dataset => {
  let checklistID = dataset.id;
  $(`#${checklistID} h3`).remove();
  $(`#${checklistID}`).prepend(
    `<div><input type="text" id="new-checklistName-${checklistID}" class="mtb" /> <button id="btn-save-checklist" data-id="${checklistID}" >Save</button> </div>`
  );
};

export const removeChecklist = checklistID => {
  $(`#${checklistID}`).remove();
};
