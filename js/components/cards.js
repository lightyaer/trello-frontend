import $ from "jquery";

export const displayCards = cards => {
  $("#boards-section").hide();
  $("#lists-section").hide();
  $("#cards-section").show();
  for (let card of cards) {
    $("#cards").append(`<div data-id="${card.id}" class="card">${card.name}</div>`);
  }
};
