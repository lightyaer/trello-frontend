import "./config/config";
import "../scss/main.scss";
import $ from "jquery";
import {
  getBoards,
  getCardsByListID,
  createCheckListForCardID,
  getChecklistsByCardID,
  getListsByBoardID,
  updateCheckListNameByCheckListID,
  deleteCheckListByID
} from "./services";
import { displayBoards } from "./components/boards";
import { displayLists } from "./components/lists";
import { displayCards } from "./components/cards";
import {
  displayChecklists,
  checkBoxChangeHandler,
  checkItemDeleteHandler,
  checkItemAddHandler,
  addNewCheckListToChecklists,
  checkItemEditHandler,
  checklistNameChangeHandler,
  removeChecklist
} from "./components/checklists";

async function init() {
  let boards = await getBoards();
  displayBoards(boards);

  $("#nav-boards").click(function() {
    $("#boards-section").show();
    $("#lists-section").hide();
    $("#cards-section").hide();
    $("#checklists-section").hide();
    $(".list").remove();
    $(".card").remove();
    $(".checklist").remove();
    $("#checklists-section #new-checklist-input").remove();
    $("#checklists-section #btn-add-checklist").remove();
  });

  $("#boards").click(async function(e) {
    $("#loader").show();
    let lists = await getListsByBoardID(e.target.dataset.id);
    displayLists(lists);
    $("#loader").hide();
  });

  $("#lists").click(async function(e) {
    $("#loader").show();
    let cards = await getCardsByListID(e.target.dataset.id);
    displayCards(cards);
    $("#loader").hide();
  });

  $("#cards").click(async function(e) {
    $("#loader").show();
    let checklists = await getChecklistsByCardID(e.target.dataset.id);
    displayChecklists(checklists);
    $("#loader").hide();
  });

  $("#checklists-section").click(async function(e) {
    switch (e.target.id) {
      case "btn-add-checklist": {
        $("#loader").show();
        let newCheckListName = $("#new-checklist-input").val();
        let cardID = e.target.dataset.cardid;
        let newChecklist = await createCheckListForCardID(cardID, newCheckListName);
        addNewCheckListToChecklists(newChecklist);
        $("#new-checklist-input").val("");
        $("#loader").hide();
        break;
      }
      case "btn-save-checklist": {
        $("#loader").show();
        let checklistID = e.target.dataset.id;
        let checklistName = $(`#new-checklistName-${checklistID}`).val();
        if (checklistName !== "" && checklistName) {
          let checklist = await updateCheckListNameByCheckListID(checklistID, checklistName);
          $(`#new-checklistName-${checklistID}`)
            .parent()
            .remove();
          $(`#${checklistID}`).prepend(`<h3 id="checklist-name" data-id="${checklistID}" >${checklist.name}</h3>`);
        }
        $("#loader").hide();
        break;
      }
      case "btn-delete-checklist": {
        $("#loader").show();
        let checklistID = e.target.dataset.checklistid;
        await deleteCheckListByID(checklistID);
        removeChecklist(checklistID);
        $("#loader").hide();
      }
      default:
        console.log(e);
        break;
    }
  });

  $(".div__checklists").click(async function(e) {
    switch (e.target.id) {
      case "checkbox":
        checkBoxChangeHandler(e.target.dataset);
        break;
      case "delete":
        checkItemDeleteHandler(e.target.dataset);
        break;
      case "btn-add":
        checkItemAddHandler(e.target.dataset);
        break;
      case "btn-edit":
        checkItemEditHandler(e.target.dataset);
        break;
      case "checklist-name":
        checklistNameChangeHandler(e.target.dataset);
        break;
      default:
        // console.log(e);
        break;
    }
  });
}

$(document).ready(init);
