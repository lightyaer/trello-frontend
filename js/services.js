const serverUrl = "https://api.trello.com/1/";

export const getBoards = async () => {
  let url = `${serverUrl}members/me/boards?key=${process.env.API_KEY}&token=${process.env.TOKEN}`;
  let response = await fetch(url, { method: "GET" });
  return response.json();
};

export const getListsByBoardID = async boardID => {
  let url = `${serverUrl}boards/${boardID}/lists?key=${process.env.API_KEY}&token=${process.env.TOKEN}`;
  let response = await fetch(url, { method: "GET" });
  return response.json();
};
export const getCardsByListID = async listID => {
  let url = `${serverUrl}lists/${listID}/cards?key=${process.env.API_KEY}&token=${process.env.TOKEN}`;
  let response = await fetch(url, { method: "GET" });
  return response.json();
};

export const getChecklistsByCardID = async cardID => {
  let url = `${serverUrl}cards/${cardID}/checklists?key=${process.env.API_KEY}&token=${process.env.TOKEN}`;
  let response = await fetch(url, { method: "GET" });
  return response.json();
};

export const getChecklistItemsByChecklistID = async checklistID => {
  let url = `${serverUrl}checklists/${checklistID}?key=${process.env.API_KEY}&token=${process.env.TOKEN}`;
  let response = await fetch(url, { method: "GET" });
  return response.json();
};
export const createCheckListForCardID = async (cardID, name) => {
  let url = `${serverUrl}checklists?idCard=${cardID}&name=${name}&pos=top&key=${process.env.API_KEY}&token=${process.env.TOKEN}`;
  let response = await fetch(url, { method: "POST" });
  return response.json();
};
export const createCheckListItemForCheckListID = async (checklistID, name, checked) => {
  let url = `${serverUrl}checklists/${checklistID}/checkItems?
  &name=${name}&pos=top&checked=${checked}&key=${process.env.API_KEY}&token=${process.env.TOKEN}`;
  let response = await fetch(url, { method: "POST" });
  return response.json();
};
export const updateCheckListNameByCheckListID = async (checklistID, name) => {
  let url = `${serverUrl}checklists/${checklistID}?&name=${name}&key=${process.env.API_KEY}&token=${process.env.TOKEN}`;
  let response = await fetch(url, { method: "PUT", headers: { "Content-Type": "application/json" } });
  return response.json();
};
export const updateCheckListItemByCardIDAndChecklistItemID = async (cardID, checklistItemID, name, state) => {
  let url = `${serverUrl}cards/${cardID}/checkItem/${checklistItemID}?&name=${name}&state=${state}&key=${process.env.API_KEY}&token=${
    process.env.TOKEN
  }`;
  let response = await fetch(url, { method: "PUT", headers: { "Content-Type": "application/json" } });
  return response.json();
};
export const deleteCheckListByID = async checklistID => {
  let url = `${serverUrl}checklists/${checklistID}?key=${process.env.API_KEY}&token=${process.env.TOKEN}`;
  let response = await fetch(url, { method: "DELETE" });
  return response.json();
};
export const deleteCheckListItemByCheckListItemID = async (checklistID, checklistItemID) => {
  let url = `${serverUrl}checklists/${checklistID}/checkItems/${checklistItemID}?key=${process.env.API_KEY}&token=${process.env.TOKEN}`;
  let response = await fetch(url, { method: "DELETE" });
  return response.json();
};
